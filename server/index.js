const express = require("express");
const cors = require("cors");
const path = require("path");
const http = require('http');
const app = express();

app.use(cors({ origin: true, credentials: true }));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, "public")));

app.use("/api", require("./api"));

// Set port
const port = process.env.PORT || '8888';
app.set('port', port);

// Create HTTP server
const server = http.createServer(app);
server.listen(port, () => {
    console.log(`Running on http://localhost:${port}`);
});
// Export the Express API
module.exports = app;