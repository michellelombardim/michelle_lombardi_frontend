import React from 'react';
import Home from './views/Home/Home';
import Search from './views/Search/Search';
import MyAlbums from './views/MyAlbums/MyAlbums';
import Navbar from './components/Navbar/Navbar';

import { BrowserRouter, Route, Routes } from 'react-router-dom';

import '@fontsource/montserrat';
import 'flexboxgrid';
import './App.scss';

function App() {
  let isLogIn = localStorage.getItem("logIn");
  return (
    <BrowserRouter >
      {isLogIn && <Navbar />}
      <Routes>
        <Route exact path='/' element={<Home />} />
        {isLogIn && <Route exact path='/buscar' element={<Search />} />}
        {isLogIn && <Route exact path='/my-albums' element={<MyAlbums />} />}
      </Routes>
    </BrowserRouter>
  );
}
export default App;
