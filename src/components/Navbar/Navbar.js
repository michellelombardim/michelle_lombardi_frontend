import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// import { DarkMode } from './../../hooks/DarkMode';
import './Navbar.scss'

export class Navbar extends Component {

    render() {
        return (
            <ul className='header row'>
                <li className='col-xs-2 col-md-2 col-lg-2'>
                    <div className='box'>
                        <Link className='headerlink green-letters' to='buscar'>Buscar</Link>
                    </div>
                </li>
                <li className='col-xs-3 col-md-3 col-lg-3'>
                    <div className='box'>
                        <Link className='headerlink white-letters' to="/my-albums">My albums</Link>
                    </div>
                </li>
                <li className='col-xs-1 col-md-1 col-lg-1'>
                    <div className='box lines'></div>
                </li>
                <li className='col-xs-3 col-md-3 col-lg-3'>
                    <div className='box'>
                        <Link className='headerlink white-letters' to="/">Cerrar sesión</Link>
                    </div>
                </li>
                <li className='col-xs-1 col-md-1 col-lg-1'>
                    <div className='box lines'></div>
                </li>
            </ul>
        )
    }
}

export default Navbar;