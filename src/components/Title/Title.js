import React, { Component } from 'react';
import './Title.scss';

export class Title extends Component {
    render() {
        let classNameFirstRow = 'row',
            classNameFirstLine = 't-line-1 color-white',
            classNameSecondLine = 't-line-2 color-green-light';
        if (this.props.jpl === "true") {
            classNameFirstLine += ' t-jpl';
            classNameSecondLine += ' t-jpl';
        } else {
            classNameFirstRow += ' t-jpc';
            classNameSecondLine += ' t-special-c-left';
        }

        return (
            <div className={classNameFirstRow}>
                <div className="col-12">
                    <div className="box-row">
                        <div className="row">
                            <div className="col-12">
                                <div className="box-row">
                                    <div className={classNameFirstLine}>{this.props.firstLine}</div>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div className="box-row">
                                    <div className={classNameSecondLine}>{this.props.secondLine}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Title;