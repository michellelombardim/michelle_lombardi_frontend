import React from 'react';
import './Album.scss';

export const Album = ({items, gb}) => {
    let classButton = 'a-button', textButton;
    if (gb === "true") {
        classButton += ' a-button-size-g color-green-light-bg color-black';
        textButton = '+ Add album';
    } else {
        classButton += ' a-button-size-r color-red-bg color-white';
        textButton = '- Remove album';
    }

    return (
        <div className="row">
            {items.map(function (result) {
                return (
                    <div key={result.id} className="col-lg-3 col-sm-6 col-md-6 col-xs-12 ma-col-margin">
                        <div className="box-row">
                            <div className="row a-jc">
                                <div className="col-12">
                                    <div className="box-row">
                                        <img className="a-img" alt="foto" src={result.images[0].url}></img>
                                    </div>
                                </div>
                            </div>
                            <div className="row a-jc">
                                <div className="col-12">
                                    <div className="box-row">
                                        <div className="a-title color-white">{result.name}</div>
                                    </div>
                                </div>
                            </div>
                            <div className="row a-jc">
                                <div className="col-12">
                                    <div className="box-row">
                                        <div className="a-sub-title color-white">Publicado: {result.release_date}</div>
                                    </div>
                                </div>
                            </div>
                            <div className="row a-jc">
                                <div className="col-12">
                                    <div className="box-row">
                                        <button id={result.id} className={classButton}>{textButton}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>)
            })}
        </div >

    )

}

export default Album;