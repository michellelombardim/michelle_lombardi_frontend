import React, { Component } from 'react';
import './SubTitle.scss';

export class SubTitle extends Component {

    render() {
        let classNameFirstRow = 'row',
            classNameDescription = 'st-msg color-white';
        if (this.props.jpl === "true") {
            classNameDescription += ' st-jpl';
            classNameDescription += ' st-special-c-left';
        } else {
            classNameFirstRow += ' st-jpc';
            classNameDescription += ' st-special-c-center';
        }

        return (
            <div className={classNameFirstRow}>
                <div className="col-12">
                    <div className="box-row">
                        <div className={classNameDescription}>
                            {this.props.description}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SubTitle;