import React from "react";
import Title from "./../../components/Title/Title";
import SubTitle from "./../../components/SubTitle/SubTitle";
import Album from "./../../components/Album/Album";

import './MyAlbums.scss';

function MyAlbums() {
    const items = [];
    return (
        <div className="row ma-go-down">
            <div className="col-12">
                <div className="box-row">
                    <div className="row ma-cj">
                        <div className="col-12">
                            <div className="box-row">
                                <Title firstLine="Mis albumes" secondLine="guardados" jpl="false"></Title>
                                <SubTitle description="Disfruta de tu música a un solo click y descube que discos has guardado dentro de  “mis  álbumes”" jpl="false"></SubTitle>
                            </div>
                        </div>
                    </div>
                    <div className="row list-go-down">
                        <div className="col-12">
                            <div className="box-row">
                                <Album items={items} gb="false"></Album>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default MyAlbums;
