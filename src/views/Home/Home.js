import React from "react";
import Title from "./../../components/Title/Title";
import SubTitle from "./../../components/SubTitle/SubTitle";

import vector from './vector.png';
import arrow from './arrow-w.png';
import './Home.scss';

import axios from "axios";

function Home() {
    const loginProcessClick = async () => {
        axios.get(`${process.env.REACT_APP_BASE_URL}/login`).then((data) => {
            window.location.replace(data.data);
        });
    };
    return (
        <div className="row">
            <div className="col-lg-6 col-sm-12 col-md-12 col-xs-12">
                <div className="box-row">
                    <img className="vector" src={vector} alt="vector" />
                </div>
            </div>
            <div className="col-lg-6 col-md-12 col-xs-12">
                <div className="box-row h-go-down">
                    <Title firstLine="Disfruta de la" secondLine="mejor música" jpl="true"></Title>
                    <SubTitle description="Accede a tu cuenta para guardar tus albumes favoritos." jpl="true"></SubTitle>
                    <div className="row">
                        <div className="col-12">
                            <div className="box-row h-margin-top-button-size">
                                <button className="h-button-log-in color-white" onClick={loginProcessClick}>
                                    Log in con Spotify
                                    <img className="arrow" src={arrow} alt="login" />
                                </button>
                            </div>
                        </div>
                    </div>
                </div >
            </div >
        </div >
    );
}

export default Home;
