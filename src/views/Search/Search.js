import React from "react";

import Title from "./../../components/Title/Title";
import SubTitle from "./../../components/SubTitle/SubTitle";
import Album from "./../../components/Album/Album";

import './Search.scss';

function Search() {
    const items = [];
    return (
        <div className="row s-go-down">
            <div className="col-12">
                <div className="box-row">
                    <Title firstLine="Busca tus" secondLine="albumes" jpl="false"></Title>
                    <SubTitle description="Encuentra tus artistas favoritos gracias a nuestro buscador y guarda tus álbumes favoritos." jpl="false"></SubTitle>
                    <div className="row">
                        <div className="col-12">
                            <div className="box-row">
                                <Album items={items} gb="true"></Album>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Search;
