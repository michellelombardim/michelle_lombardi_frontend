import { useState, useEffect } from "react";
import axios from "axios";

export default function useAuth(code) {
    const [accessToken, setAccessToken] = useState();
    const [refreshToken, setRefreshToken] = useState();
    const [expiresIn, setExpiresIn] = useState();
    useEffect(() => {
        console.log("useEffect:    " + code)
        axios.post('http://localhost:3001/login', { params: { code } })
            .then((data) => {
                localStorage.setItem("logIn", true);
                setAccessToken(data.access_token)
                setRefreshToken(data.efresh_token)
                setExpiresIn(data.expires_in)
                window.history.pushState({}, null, "/")
            })
            .catch((err) => {
                console.log(err)
                localStorage.removeItem("logIn");
                //window.location = "/";
            });
    }, [code]);

    useEffect(() => {
        if (!refreshToken || !expiresIn) return;
        const interval = setInterval(() => {
            axios.post('http://localhost:3001/refresh', { refreshToken })
                .then((data) => {
                    localStorage.setItem("login", true);
                    setAccessToken(data.access_token)
                    setExpiresIn(data.expires_in)
                })
                .catch((err) => {
                    localStorage.removeItem("logIn");
                    window.location = "/";
                });
        }, (expiresIn - 60) * 1000);

        return () => clearInterval(interval);
    }, [refreshToken, expiresIn]);

    return accessToken;
}
