import { render, screen } from '@testing-library/react';
import App from './App';

test('renders first message', () => {
  render(<App />);
  const textElement = screen.getByText(/Hola mundo!/i);
  expect(textElement).toBeInTheDocument();
});
